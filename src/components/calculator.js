import React, { useState } from "react";
import './calculator.css';

function Calci() {


    const [calc, setCalc] = useState('');
    const [result, setResult] = useState('');

    const ops = ['/', '*', '+', '-', '.'];

    const updateCalc = value => {
        if (
            ops.includes(value) && calc === '' ||
            ops.includes(value) && ops.includes(calc.slice(-1))
        ) {
            return;
        }

        setCalc(calc + value);
        if (!ops.includes(value)) {
            setResult(eval(calc + value).toString());
        }
    }


    const createDigits = () => {
        const digits = [];
        for (let i = 1; i < 10; i++) {
            digits.push(
                <div className='btn' onClick={() => updateCalc(i.toString())} key={i}>{i}</div>
            )
        }
        return digits;
    }

    const calculate = () => {
        setCalc(eval(calc).toString());
    }

    const deleteLast = () => {
        if (calc == '') { 
            return;
        }
        const value = calc.slice(0, -1);
        setCalc(value);
    }

    const reset = () => {
       setResult('');
       setCalc('');
    };

    const minusPlus = () => {
        if (calc.charAt(0) === "-"){
          setCalc(calc.substring(1));
        }else{
          setCalc("-" + calc);
        }
       };

    return (
        <div className='body'>
            <div className='container'>
                <div className='wrapper'>
                    <div className='screen'>
                        {result ? <span>({result})</span> : ''}&nbsp;
                        {calc || '0'}
                    </div>
                    {createDigits()}
                    <div className='btn light-gray' onClick={minusPlus}>+/-</div>
                    <div className='btn light-gray' onClick={deleteLast}>DEL</div>
                    <div className='btn light-gray ' onClick={reset}>AC</div>
                    <div className='btn orange' onClick={() => updateCalc('/')}>/</div>
                    <div className='btn orange' onClick={() => updateCalc('*')}>*</div>
                    <div className='btn orange' onClick={() => updateCalc('+')}>+</div>
                    <div className='btn orange' onClick={() => updateCalc('-')}>-</div>
                    <div className='btn zero' onClick={() => updateCalc('0')}>0</div>
                    <div className='btn' onClick={() => updateCalc('.')}>.</div>
                    <div className='btn' onClick={calculate}>=</div>
                </div>
            </div>
        </div>
    );
}

export default Calci;