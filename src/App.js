import Calci from "./components/calculator";



function App() {
  return (
    <div className="App">
      <Calci />
    </div>
  );
}

export default App;
